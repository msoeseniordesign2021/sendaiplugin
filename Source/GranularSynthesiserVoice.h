#ifndef GRANULAR_SYNTHESISER_VOICE_H
#define GRANULAR_SYNTHESISER_VOICE_H

#include "JuceHeader.h"
#include "GranularSynthesiserSound.h"
#include "Grain.h"

using namespace juce;

class GranularSynthesiserVoice : public  SynthesiserVoice
{

public:
    //==============================================================================
    /** Creates a GranularSynthesiserVoice. */
    GranularSynthesiserVoice();

    /** Destructor. */
    ~GranularSynthesiserVoice() override;

    //==============================================================================
    bool canPlaySound(SynthesiserSound*) override;

    void startNote(int midiNoteNumber, float velocity, SynthesiserSound*, int pitchWheel) override;
    void stopNote(float velocity, bool allowTailOff) override;

    void pitchWheelMoved(int newValue) override {}
    void controllerMoved(int controllerNumber, int newValue) override {}

    void renderNextBlock(AudioBuffer<float>&, int startSample, int numSamples) override;
    using SynthesiserVoice::renderNextBlock;

private:
    //==============================================================================

    /**
    *\fn: RandomizeGrain
    *\brief: Randomizes a Grain
    *\param: Grain& grain - Grain Parameters to Randomize
    */
    void randomizeGrain(Grain& grain, const GranularSynthesiserSound& sound);

    void addGrain(GranularSynthesiserSound const* sound);

    float getNextSample(GranularSynthesiserSound const* sound, int sourceChannel);


    std::vector<Grain> grains;

    double pitchRatio = 0;
    float gain = 0;
    bool inRelease = true;

    JUCE_LEAK_DETECTOR(GranularSynthesiserVoice)
};

#endif //GRANULAR_SYNTHESISER_VOICE_H
