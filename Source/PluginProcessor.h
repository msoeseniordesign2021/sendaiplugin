/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#ifndef PLUGIN_PROCESSOR_H
#define PLUGIN_PROCESSOR_H

#include <JuceHeader.h>
#include "GranularVisualizer.h"

//==============================================================================
/**
*/
class SendaiPluginAudioProcessor : public juce::AudioProcessor
{
public:
    //==============================================================================
    SendaiPluginAudioProcessor(File samplesDirectory = File());
    ~SendaiPluginAudioProcessor() override;

    //==============================================================================
    void prepareToPlay(double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

#ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported(const BusesLayout& layouts) const override;
#endif

    void processBlock(juce::AudioBuffer<float>&, juce::MidiBuffer&) override;

    //==============================================================================
    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram(int index) override;
    const juce::String getProgramName(int index) override;
    void changeProgramName(int index, const juce::String& newName) override;

    //==============================================================================
    void getStateInformation(juce::MemoryBlock& destData) override;
    void setStateInformation(const void* data, int sizeInBytes) override;
    std::unique_ptr<XmlElement> getStateInformationXml();
    void setStateInformationXml(XmlElement& xml);


    void setSampledSound(File& sample);

    GranularVisualizer& getVisualizer();

    juce::AudioProcessorValueTreeState valueState;

    int getCurrentSampleRate() const;
    int getCurrentSampleLength() const;

    void startRecording(const File& file);

    void stop();

    bool isRecording() const;

    void setFeedbackEnabled(bool enabled);

    void setRecordChannelToUse(int channelToUse);



private:
    //==============================================================================

    Synthesiser synth;
    File samplesDirectory;

    AudioFormatManager audioFormatManager;
    AudioThumbnailCache thumbnailCache; //<! Drawing Cache for the Waveform
    AudioThumbnail thumbnail;           //<! Waveform Thumbnail Object

    GranularVisualizer visualizer;

    TimeSliceThread backgroundThread{ "Audio Recorder Thread" }; // the thread that will write our audio data to disk
    std::unique_ptr<AudioFormatWriter::ThreadedWriter> threadedWriter; // the FIFO used to buffer the incoming data
    int64 nextSampleNum = 0;

    CriticalSection writerLock;
    std::atomic<AudioFormatWriter::ThreadedWriter*> activeWriter{ nullptr };

    int sampleRate;
    int sampleLength;
    int recordChannelToUse;
    
    bool feedbackEnabled = false;

    juce::AudioProcessorValueTreeState::ParameterLayout createParams();


    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SendaiPluginAudioProcessor)
};

#endif
