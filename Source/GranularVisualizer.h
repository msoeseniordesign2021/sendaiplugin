/*
  ==============================================================================

    GranularVisualizer.h
    Created: 14 Jan 2022 10:20:17am
    Author:  Danny

  ==============================================================================
*/

#ifndef GRANULAR_VISUALIZER_H
#define GRANULAR_VISUALIZER_H

#include "JuceHeader.h"

class SendaiPluginAudioProcessor;

class GranularVisualizer : public Component, public AudioProcessorValueTreeState::Listener, public ChangeListener
{
public:

    GranularVisualizer(SendaiPluginAudioProcessor& processor, AudioThumbnail& thumbnail);

    ~GranularVisualizer() override;

    void parameterChanged(const String& parameterID, float newValue) override;
    void changeListenerCallback(ChangeBroadcaster* source) override;

    void paint(juce::Graphics& g) override;

private:
    SendaiPluginAudioProcessor& processor;
    AudioThumbnail& thumbnail;
};

#endif
