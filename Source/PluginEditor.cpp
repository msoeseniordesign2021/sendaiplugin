/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
SendaiPluginAudioProcessorEditor::SendaiPluginAudioProcessorEditor(SendaiPluginAudioProcessor& p)
    : AudioProcessorEditor(&p),
    audioProcessor(p),
    fileChooser("Choose sample"),
    selectSampleButton("Select sample"),
    recordButton("Record"),
    saveButton("Save sample"),
    feedbackEnabledButton("Feedback Toggle"),
    granularVisualizer(p.getVisualizer())
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize(800, 700);


    auto& processorValues = p.valueState;

    //------ CENTROID SAMPLE -------//

// Label
    addAndMakeVisible(centroidSampleLabel);
    centroidSampleLabel.setText("Centroid Sample Location", dontSendNotification);
    centroidSampleLabel.attachToComponent(&centroidSampleSlider, false);
    centroidSampleLabel.setJustificationType(Justification::centred);

    // Slider

    //centroidSampleSlider.setRange(1, 2);
    centroidSampleSlider.setTextValueSuffix(" Sample");
    centroidSampleSlider.setNumDecimalPlacesToDisplay(3);
    centroidSampleAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processorValues, "CENTROIDSAMPLE", centroidSampleSlider);
    addAndMakeVisible(centroidSampleSlider);

    //------ GRAIN DURATION -------//

    // Label
    addAndMakeVisible(grainDurationLabel);
    grainDurationLabel.setText("Grain Duration", dontSendNotification);
    grainDurationLabel.attachToComponent(&grainDurationSlider, false);
    grainDurationLabel.setJustificationType(Justification::centred);

    // Slider
    //grainDurationSlider.setRange(1, 1000);
    grainDurationSlider.setTextValueSuffix(" ms");
    grainDurationSlider.setNumDecimalPlacesToDisplay(3);
    grainDurationAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processorValues, "GRAINDURATION", grainDurationSlider);
    addAndMakeVisible(grainDurationSlider);

    //------ CLOUD SIZE -------//

    // Label
    addAndMakeVisible(cloudSizeLabel);
    cloudSizeLabel.setText("Cloud Size", dontSendNotification);
    cloudSizeLabel.attachToComponent(&cloudSizeSlider, false);
    cloudSizeLabel.setJustificationType(Justification::centred);

    // Slider
    //cloudSizeSlider.setRange(1, 10);
    cloudSizeSlider.setTextValueSuffix(" grains");
    cloudSizeSlider.setNumDecimalPlacesToDisplay(0);
    cloudSizeAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processorValues, "CLOUDSIZE", cloudSizeSlider);
    addAndMakeVisible(cloudSizeSlider);

    //------ STARTING OFFSET -------//

    // Label
    addAndMakeVisible(startingOffsetLabel);
    startingOffsetLabel.setText("Random Sample Offset", dontSendNotification);
    startingOffsetLabel.attachToComponent(&startingOffsetSlider, false);
    startingOffsetLabel.setJustificationType(Justification::centred);

    // Slider
    //startingOffsetSlider.setRange(0, 10000);
    startingOffsetSlider.setTextValueSuffix(" samples");
    startingOffsetSlider.setNumDecimalPlacesToDisplay(0);
    startingOffsetAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processorValues, "RANDOMOFFSET", startingOffsetSlider);
    addAndMakeVisible(startingOffsetSlider);


    selectSampleButton.onClick = [&]() {selectSampleClicked(); };
    addAndMakeVisible(selectSampleButton);

    recordButton.onClick = [&] {recordClicked(); };
    addAndMakeVisible(recordButton);

    //saveButton.onClick = [&] {saveClicked()};
    addAndMakeVisible(saveButton);

    addAndMakeVisible(feedbackEnabledButton);
    feedbackEnabledButton.onClick = [&]() {toggleFeedbackClicked(); };


    addAndMakeVisible(granularVisualizer);
    setResizable(true, true);
}

SendaiPluginAudioProcessorEditor::~SendaiPluginAudioProcessorEditor()
{
}

void SendaiPluginAudioProcessorEditor::selectSampleClicked()
{
    fileChooser.launchAsync(FileBrowserComponent::canSelectFiles, [&](const FileChooser& chooser)
        {
            auto chosen = chooser.getResult();
            if (chosen.exists())
            {
                audioProcessor.setSampledSound(chosen);
            }
        });
}

void SendaiPluginAudioProcessorEditor::recordClicked()
{
    if (audioProcessor.isRecording())
    {
        audioProcessor.stop();
        audioProcessor.setSampledSound(File::getCurrentWorkingDirectory().getChildFile("./.tmp.wav"));
        recordButton.setButtonText("Record");
    }
    else
    {
        recordButton.setButtonText("Stop Recording");
        audioProcessor.startRecording(File::getCurrentWorkingDirectory().getChildFile("./.tmp.wav"));
    }
}

void SendaiPluginAudioProcessorEditor::toggleFeedbackClicked()
{
    static bool enabled = false;
    enabled = !enabled;
    audioProcessor.setFeedbackEnabled(enabled);
}

//==============================================================================
void SendaiPluginAudioProcessorEditor::paint(juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll(getLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId));

    g.setColour(juce::Colours::white);
    g.setFont(15.0f);
}

void SendaiPluginAudioProcessorEditor::resized()
{
    int yValue = 30;
    int yValueOffset = 50;
    auto xValue = 50;
    auto halfWidth = (getWidth() / 2) - 30;

    // Centroid Sample
    centroidSampleSlider.setBounds(xValue, (yValue), getWidth() - xValue - 10, 20);

    // Grain Duration
    grainDurationSlider.setBounds(xValue, (yValue += yValueOffset), getWidth() - xValue - 10, 20);

    // Cloud Size
    cloudSizeSlider.setBounds(xValue, (yValue += yValueOffset), getWidth() - xValue - 10, 20);

    // Offset
    startingOffsetSlider.setBounds(xValue, (yValue += yValueOffset), getWidth() - xValue - 10, 20);

    selectSampleButton.setBounds(xValue, (yValue += yValueOffset), getWidth() - xValue - 10, 20);

    recordButton.setBounds(xValue, (yValue += yValueOffset), getWidth() - xValue - 10, 20);

    saveButton.setBounds(xValue, (yValue += yValueOffset), getWidth() - xValue - 10, 20);

    feedbackEnabledButton.setBounds(xValue, (yValue += yValueOffset), getWidth() - xValue - 10, 20);


    maxYValue = (yValue + 50);

    granularVisualizer.setBounds(20, maxYValue, getWidth() - 40, (getHeight() / 4));
}
