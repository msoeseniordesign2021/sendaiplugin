/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#ifndef PLUGIN_EDITOR_H
#define PLUGIN_EDITOR_H

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "GranularVisualizer.h"

//==============================================================================
/**
*/
class SendaiPluginAudioProcessorEditor : public juce::AudioProcessorEditor
{
public:
    SendaiPluginAudioProcessorEditor(SendaiPluginAudioProcessor&);
    ~SendaiPluginAudioProcessorEditor() override;

    //==============================================================================
    void paint(juce::Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    SendaiPluginAudioProcessor& audioProcessor;

    GranularVisualizer& granularVisualizer;

    TextButton selectSampleButton;
    TextButton recordButton;
    TextButton saveButton;

    FileChooser fileChooser;

    Label centroidSampleLabel;   //<! Label Noting the Centroid Sample Slider
    Slider centroidSampleSlider; //<! Slider Designating the Starting Sample of a Grain
    std::unique_ptr<AudioProcessorValueTreeState::SliderAttachment> centroidSampleAttachment;

    Label grainDurationLabel;   //<! Label Noting the Duration Slider
    Slider grainDurationSlider; //<! Slider Designating the Duration of a Grain
    std::unique_ptr<AudioProcessorValueTreeState::SliderAttachment> grainDurationAttachment;

    Label startingOffsetLabel;   //<! Label Noting the Starting Offset Slider
    Slider startingOffsetSlider; //<! Slider Designating the Offset a Grain can use.
    std::unique_ptr<AudioProcessorValueTreeState::SliderAttachment> startingOffsetAttachment;

    Label cloudSizeLabel;   //<! Label Noting the Cloud Size Slider
    Slider cloudSizeSlider; //<! Slider Designating the Cloud Size of a Grain
    std::unique_ptr<AudioProcessorValueTreeState::SliderAttachment> cloudSizeAttachment;

    ToggleButton feedbackEnabledButton;

    int maxYValue = 0; //<! Maximum Y value of Components being Drawn to the Screen

    void selectSampleClicked();
    void recordClicked();
    void toggleFeedbackClicked();


    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SendaiPluginAudioProcessorEditor)
};

#endif
