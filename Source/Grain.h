/*
  ==============================================================================

    Grain.h
    Created: 22 Dec 2021 2:53:47pm
    Author:  Danny

  ==============================================================================
*/
#include "JuceHeader.h"

#pragma once


struct Grain
{
    Grain(int sampleRate)
    {
        envelope.setSampleRate(sampleRate);
        envelope.setParameters(juce::ADSR::Parameters(0.01, 0.1, 1.0, 0.1));
    }

    void ResetStartSample(int startingSample)
    {
        currentSample[0] = startingSample;
        currentSample[1] = startingSample;
    }

    //! Current Playing Sample of a Grain
    double currentSample[2] = { 0.0, 0.0 };
    bool inRelease = true; //!< Boolean for whether or not the Grain needs to be replayed.

    int startSample = 0; //!< Actual Starting Sample for a Specific Grain
    int endSample = 0;      //!< Ending Sample of a Grain

    juce::ADSR envelope; //!< ADSR Envelope for a Grain
};
