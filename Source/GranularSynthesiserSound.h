#ifndef GRANULAR_SYNTHESISER_SOUND_H
#define GRANULAR_SYNTHESISER_SOUND_H

#include "JuceHeader.h"
using namespace juce;

constexpr auto LEFT_CHANNEL = 0;
constexpr auto RIGHT_CHANNEL = 1;

class GranularSynthesiserSound : public SynthesiserSound, public AudioProcessorValueTreeState::Listener
{

public:
    /**
   *\Grain constructor
   *\brief: Generates a Grain from a waveform generator
   *\param: int StartingSample - Sample to Start the Grain
   *\param: int Duration - Duration of the Grain
   */
    GranularSynthesiserSound(int startingSample_ = 1, int duration = 0);

    /** Destructor. */
    ~GranularSynthesiserSound() override;

    // SynthesiserSound
    //==============================================================================
    /** Returns true if this sound should be played when a given midi note is pressed.

        The Synthesiser will use this information when deciding which sounds to trigger
        for a given note.
    */
    bool appliesToNote(int midiNoteNumber) { return true; }

    /** Returns true if the sound should be triggered by midi events on a given channel.

        The Synthesiser will use this information when deciding which sounds to trigger
        for a given note.
    */
    bool appliesToChannel(int midiChannel) { return true; }


    void parameterChanged(const String& parameterID, float newValue) override;

    // GranularSynthesiserSound
    //==============================================================================

    static const String CENTROID_SAMPLE_ID;
    const static String GRAIN_DURATION_ID;
    const static String CLOUD_SIZE_ID;
    const static String RANDOM_OFFSET_ID;

    /**
   *\Grain () operator
   *\brief: Plays a sample of the Grains (from WAV form)
   *\param: int channel - Channel of an Audio Buffer to Play Sample From
   *\return: float - Current Sample of the Grain
   */
   // float operator()(int channel); belongs in voice

   /**
  *\fn: SetStartingSample
  *\brief: Sets the Starting Sample of a Grain
  *\param: int startingSample - Sample to Start the Grain
  */
    void setCentroidSample(float startingSample);

    /*
   *\fn: GetStartingSample
   *\brief: Gets the Starting Sample of a Grain
   *\return: int - Sample where the grain starts
   */
    int getCentroidSample() { return centroidSample; }

    /**
   *\fn: SetDuration
   *\brief: Sets the Duration of the Grain, and updates the Sample Delta and Ending Sample
   *\param: int duration - duration (in ms) of the Grain
   */
    void setDuration(int ms);

    /**
   *\fn: SetAudioSource
   *\brief: Sets the Audio Source of the Grain File and Updates The Grain's Properties
   *\param: AudioFormatReader& audioReader - Reader Containing The New Audio File
   */
    void setAudioSource(AudioFormatReader& audioReaderm, int midiRootNote);

    /**
   *\fn: SetCloudSize
   *\brief: Sets the Cloud Size
   *\param: int size - number of grains in the cloud
   */
    void setNumGrains(int size);

    /**
   *\fn: GetSize
   *\brief: Get's the Size of the Grain in Samples
   *\return: int - Size of the Grain in Samples
   */
    int getSampleLength() { return waveSize; };

    /**
   *\fn: Reset
   *\brief: Resets the Grain to its inital state
   */
    void reset();

    /**
   *\fn: HasValidWAVFile
   *\brief: Checks if the Grain Has a Valid WAV File
   *\return: Boolean - Valid WAV file
   */
    bool getHasValidWavFile()
    {
        return this->hasValidWavFile;
    }


    //! Offset of where a Grain can start from
    int startingOffset = 0;

    //! Scalar Value for the Release Duration of the ADSR (in ms)
    double envelopeRelease = 100.0;

    //! Sampling Rate of the Active Grain
    double sampleRate = 0.0;




private:
    //================================VARIABLES=====================================
    friend class GranularSynthesiserVoice;

    //! AudioSampleBuffer of the Grain's Audio Source WAV file.
    std::unique_ptr<AudioSampleBuffer> sourceAudioBuffer;

    int waveSize = 0; //!< The Size of the Audio Waveform being used

    int cloudSize = 0; //!< Size of the Grain Cloud (Number of Grains to Play)

    float centroidPercentage;
    int centroidSample = 1; //!< The Centroid Sample for a Grain Cloud
    int duration = 0;       //!< Duration (in ms) of a Grain Cloud
    int sampleDelta = 0;    //!< Delta Between the Starting Sample and the Ending Sample (determined by Duration)
    bool hasValidWavFile; //!< Boolean for whether or not the Grain has a valid WAV file.
    int midiRootNote = 0;

    //================================FUNCTIONS=====================================

    /**
   *\fn: AddGrains
   *\brief: Adds Grains from the GrainCloud
   *\param: int count - number of grains to add (default - 1)
   */
    void addGrains(int count = 1);

    /**
   *\fn: RemoveGrains
   *\brief: Removes Grains from the GrainCloud
   *\param: int count - number of grains to remove (default - 1)
   */
    void removeGrains(int count = 1);
};

#endif //GRANULAR_SYNTHESISER_SOUND_H
