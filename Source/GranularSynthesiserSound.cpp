/*****************************************************************************/
/*!
\file   Grain.cpp
\author Anthony Brigante
\par    email: anthonypbrigante\@gmail.com
\par    GitHub Repository: https://github.com/abrigante1/GranularSynth
\brief
  This is the Implementation of the GranularSynthesiserSound Class
*/
/*****************************************************************************/

#include "GranularSynthesiserSound.h"


const String GranularSynthesiserSound::CENTROID_SAMPLE_ID = "CENTROIDSAMPLE";
const String GranularSynthesiserSound::GRAIN_DURATION_ID = "GRAINDURATION";
const String GranularSynthesiserSound::CLOUD_SIZE_ID = "CLOUDSIZE";
const String GranularSynthesiserSound::RANDOM_OFFSET_ID = "RANDOMOFFSET";

// ------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------

GranularSynthesiserSound::GranularSynthesiserSound(int startingSample_, int duration_)
{
    sampleRate = 0;
    setDuration(duration_);
    setCentroidSample(startingSample_);
    addGrains();
}

GranularSynthesiserSound::~GranularSynthesiserSound()
{

}

// ------------------------------------------------------------------------------------

void GranularSynthesiserSound::setCentroidSample(float percent)
{
    // Update the Starting Sample, and Reset the Current Sample
    centroidPercentage = percent;
    if (hasValidWavFile)
    {
        centroidSample = percent * waveSize;
        if (percent == 1) --centroidSample;
    }

    /*centroidSample = startingSample - 1;*/
}

// ------------------------------------------------------------------------------------

void GranularSynthesiserSound::setDuration(int ms)
{
    // Update Duration and Calculate mSample Delta
    this->duration = ms;
    sampleDelta = static_cast<int>(sampleRate * (static_cast<float>(duration) / 1000.0f));
}

// ------------------------------------------------------------------------------------

void GranularSynthesiserSound::setAudioSource(AudioFormatReader& newAudioFile, int midiRootNote)
{
    // Update Grain Parameters
    waveSize = static_cast<int>(newAudioFile.lengthInSamples);
    sampleRate = static_cast<int>(newAudioFile.sampleRate);
    hasValidWavFile = true;
    setCentroidSample(centroidPercentage);
    setDuration(duration);

    // Clear the Audio Source and Read the New WAV File
    sourceAudioBuffer.reset(new AudioSampleBuffer(newAudioFile.numChannels, static_cast<int>(newAudioFile.lengthInSamples)));
    newAudioFile.read(sourceAudioBuffer.get(), 0, static_cast<int>(newAudioFile.lengthInSamples), 0, true, true);
    this->midiRootNote = midiRootNote;
}

// ------------------------------------------------------------------------------------

void GranularSynthesiserSound::setNumGrains(int size)
{
    if (size > cloudSize)
        addGrains(size - cloudSize);
    else if (size < cloudSize)
        removeGrains(cloudSize - size);
}

// ------------------------------------------------------------------------------------

void GranularSynthesiserSound::parameterChanged(const String& parameterID, float newValue)
{
    if (parameterID == CENTROID_SAMPLE_ID)
    {
        setCentroidSample(newValue);
    }
    else if (parameterID == CLOUD_SIZE_ID)
    {
        setNumGrains((int)newValue);
    }
    else if (parameterID == GRAIN_DURATION_ID)
    {
        // juce::Logger::writeToLog("Grain duration increased to "+ String(newValue));
        setDuration(newValue);
    }
    else if (parameterID == RANDOM_OFFSET_ID)
    {
        startingOffset = (int)newValue;
    }
}



// ------------------------------------------------------------------------------------

void GranularSynthesiserSound::addGrains(int count)
{
    cloudSize += count;
}

// ------------------------------------------------------------------------------------

void GranularSynthesiserSound::removeGrains(int count)
{
    cloudSize -= count;
}

// ------------------------------------------------------------------------------------


void GranularSynthesiserSound::reset()
{

}

// ------------------------------------------------------------------------------------
