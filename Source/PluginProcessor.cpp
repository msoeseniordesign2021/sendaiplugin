/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "GranularSynthesiserVoice.h"

//==============================================================================
SendaiPluginAudioProcessor::SendaiPluginAudioProcessor(File samplesDirectory)
    : AudioProcessor(BusesProperties()
#if ! JucePlugin_IsMidiEffect
#if ! JucePlugin_IsSynth
        .withInput("Input", juce::AudioChannelSet::stereo(), true)
#endif
        .withOutput("Output", juce::AudioChannelSet::stereo(), true)
#endif
    ),
    valueState(*this, nullptr, "Parameters", createParams()),
    thumbnailCache(5),
    thumbnail(512, audioFormatManager, thumbnailCache),
    visualizer(*this, thumbnail),
    samplesDirectory(samplesDirectory)
{
    //valueState.state.setProperty("SAMPLENAME", "newFile", nullptr);

    for (auto i = 0; i < 8; ++i)
    {
        synth.addVoice(new GranularSynthesiserVoice()); // granular synthesiser
    }

    audioFormatManager.registerBasicFormats();

    auto sound = new GranularSynthesiserSound();

    valueState.addParameterListener(GranularSynthesiserSound::CENTROID_SAMPLE_ID, sound);
    valueState.addParameterListener(GranularSynthesiserSound::GRAIN_DURATION_ID, sound);
    valueState.addParameterListener(GranularSynthesiserSound::CLOUD_SIZE_ID, sound);
    valueState.addParameterListener(GranularSynthesiserSound::RANDOM_OFFSET_ID, sound);

    synth.addSound(sound);

    backgroundThread.startThread();
}

SendaiPluginAudioProcessor::~SendaiPluginAudioProcessor()
{
}

//==============================================================================
const juce::String SendaiPluginAudioProcessor::getName() const
{
    return "Sendai Plugin";
}

bool SendaiPluginAudioProcessor::acceptsMidi() const
{
#if JucePlugin_WantsMidiInput
    return true;
#else
    return false;
#endif
}

bool SendaiPluginAudioProcessor::producesMidi() const
{
#if JucePlugin_ProducesMidiOutput
    return true;
#else
    return false;
#endif
}

bool SendaiPluginAudioProcessor::isMidiEffect() const
{
#if JucePlugin_IsMidiEffect
    return true;
#else
    return false;
#endif
}

double SendaiPluginAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int SendaiPluginAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int SendaiPluginAudioProcessor::getCurrentProgram()
{
    return 0;
}

void SendaiPluginAudioProcessor::setCurrentProgram(int index)
{
}

const juce::String SendaiPluginAudioProcessor::getProgramName(int index)
{
    return {};
}

void SendaiPluginAudioProcessor::changeProgramName(int index, const juce::String& newName)
{
}

//==============================================================================
void SendaiPluginAudioProcessor::prepareToPlay(double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
    synth.setCurrentPlaybackSampleRate(sampleRate);
}

void SendaiPluginAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool SendaiPluginAudioProcessor::isBusesLayoutSupported(const BusesLayout& layouts) const
{
#if JucePlugin_IsMidiEffect
    juce::ignoreUnused(layouts);
    return true;
#else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
        && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
#if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
#endif

    return true;
#endif
}
#endif

void SendaiPluginAudioProcessor::processBlock(juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    //recording
    const ScopedLock sl(writerLock);
    if (activeWriter.load() != nullptr && totalNumInputChannels >= thumbnail.getNumChannels())
    {
        auto channelPtr = &buffer.getArrayOfWritePointers()[recordChannelToUse];
        activeWriter.load()->write(channelPtr, buffer.getNumSamples());
        
        thumbnail.addBlock(nextSampleNum, AudioBuffer<float>(channelPtr, 1, buffer.getNumSamples()), 0, buffer.getNumSamples());
        nextSampleNum += buffer.getNumSamples();
    }



    //clear out inputs if feedback disabled
    // if (!feedbackEnabled)
    //     for (auto i = 0; i < totalNumOutputChannels; ++i)
    //         buffer.clear(i, 0, buffer.getNumSamples());

        for (auto i = 0; i < totalNumOutputChannels; ++i)
        {
            if (feedbackEnabled && i == recordChannelToUse) continue;
            buffer.clear(i, 0, buffer.getNumSamples());
        }


    //synth output
    synth.renderNextBlock(buffer, midiMessages, 0, buffer.getNumSamples());
}

void SendaiPluginAudioProcessor::startRecording(const File& file)
{
    stop();

    if (getSampleRate() > 0)
    {
        // Create an OutputStream to write to our destination file...
        file.deleteFile();

        if (auto fileStream = std::unique_ptr<FileOutputStream>(file.createOutputStream()))
        {
            // Now create a WAV writer object that writes to our output stream...
            WavAudioFormat wavFormat;

            if (auto writer = wavFormat.createWriterFor(fileStream.get(), getSampleRate(), 1, 32, {}, 0))
            {
                fileStream.release(); // (passes responsibility for deleting the stream to the writer object that is now using it)

                // Now we'll create one of these helper objects which will act as a FIFO buffer, and will
                // write the data to disk on our background thread.
                threadedWriter.reset(new AudioFormatWriter::ThreadedWriter(writer, backgroundThread, 32768));

                // Reset our recording thumbnail
                thumbnail.reset(writer->getNumChannels(), writer->getSampleRate());
                nextSampleNum = 0;

                // And now, swap over our active writer pointer so that the audio callback will start using it..
                const ScopedLock sl(writerLock);
                activeWriter = threadedWriter.get();
            }
        }
    }
}

void SendaiPluginAudioProcessor::stop()
{
    // First, clear this pointer to stop the audio callback from using our writer object..
    {
        const ScopedLock sl(writerLock);
        activeWriter = nullptr;
    }

    // Now we can delete the writer object. It's done in this order because the deletion could
    // take a little time while remaining data gets flushed to disk, so it's best to avoid blocking
    // the audio callback while this happens.
    threadedWriter.reset();
}

bool SendaiPluginAudioProcessor::isRecording() const
{
    return activeWriter.load() != nullptr;
}

void SendaiPluginAudioProcessor::setFeedbackEnabled(bool enabled)
{
    feedbackEnabled = enabled;
}

void SendaiPluginAudioProcessor::setRecordChannelToUse(int channelToUse)
{
    recordChannelToUse = channelToUse;
}


//==============================================================================
bool SendaiPluginAudioProcessor::hasEditor() const
{
#ifndef SENDAI
    return true;
#else
    return false;
#endif
}

juce::AudioProcessorEditor* SendaiPluginAudioProcessor::createEditor()
{
#ifndef SENDAI
    return new SendaiPluginAudioProcessorEditor(*this);
#else
    return nullptr;
#endif
}

//==============================================================================
void SendaiPluginAudioProcessor::getStateInformation(juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
    copyXmlToBinary(*getStateInformationXml(), destData);
}

std::unique_ptr<XmlElement> SendaiPluginAudioProcessor::getStateInformationXml()
{
    return valueState.state.createXml();
}

void SendaiPluginAudioProcessor::setStateInformationXml(XmlElement & xml)
{
    valueState.replaceState(ValueTree::fromXml(xml));
    auto& path = valueState.state.getProperty("SAMPLE");

    auto file = samplesDirectory.isDirectory() ? samplesDirectory.getChildFile(path.toString()) : File(path.toString());
    if (file.existsAsFile())
    {
        setSampledSound(file);
    }
}


void SendaiPluginAudioProcessor::setStateInformation(const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
    auto xml = getXmlFromBinary(data, sizeInBytes);
    setStateInformationXml(*xml);
}


GranularVisualizer& SendaiPluginAudioProcessor::getVisualizer()
{
    return visualizer;
}


int SendaiPluginAudioProcessor::getCurrentSampleRate() const
{
    return sampleRate;
}
int SendaiPluginAudioProcessor::getCurrentSampleLength() const
{
    return sampleLength;
}

void SendaiPluginAudioProcessor::setSampledSound(File& file)
{
    jassert(file.existsAsFile());

    std::unique_ptr<AudioFormatReader> audioReader(audioFormatManager.createReaderFor(file.createInputStream()));
    AudioFormatReaderSource frSrc(audioReader.get(), false);

    thumbnail.setSource(new FileInputSource(file));

    sampleRate = audioReader->sampleRate;
    sampleLength = audioReader->lengthInSamples;



    auto sound = (GranularSynthesiserSound*)synth.getSound(0).get();

    sound->setAudioSource(*audioReader, 74);

    auto newVal = file.getRelativePathFrom(File::getCurrentWorkingDirectory());

    auto samplePath = samplesDirectory.isDirectory() ? file.getFileName() : file.getFullPathName();

    valueState.state.setProperty("SAMPLE", samplePath, nullptr);
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new SendaiPluginAudioProcessor();
}

juce::AudioProcessorValueTreeState::ParameterLayout SendaiPluginAudioProcessor::createParams()
{
    std::vector<std::unique_ptr<juce::RangedAudioParameter>> params;

    params.push_back(std::make_unique<juce::AudioParameterFloat>(GranularSynthesiserSound::CENTROID_SAMPLE_ID, "Centroid Sample Location", 0, 1, 0));
    params.push_back(std::make_unique<juce::AudioParameterFloat>(GranularSynthesiserSound::GRAIN_DURATION_ID, "Grain Duration", 0, 999999999999999999999999999999.0, 10));
    params.push_back(std::make_unique<juce::AudioParameterInt>(GranularSynthesiserSound::CLOUD_SIZE_ID, "Cloud Size", 1, 100, 1));
    params.push_back(std::make_unique<juce::AudioParameterFloat>(GranularSynthesiserSound::RANDOM_OFFSET_ID, "Random Sample Offset", 0, 999999999999999999999999999999.0, 0));

    return { params.begin(), params.end() };
}
