#include "GranularSynthesiserVoice.h"
#include "GranularSynthesiserSound.h"
using namespace juce;

#ifndef M_PI_2
#define M_PI_2 1.57079632679489661923
#endif

//==============================================================================
GranularSynthesiserVoice::GranularSynthesiserVoice() {}
GranularSynthesiserVoice::~GranularSynthesiserVoice() {}

bool GranularSynthesiserVoice::canPlaySound(SynthesiserSound* sound)
{
    return dynamic_cast<const GranularSynthesiserSound*>(sound) != nullptr;
}


void GranularSynthesiserVoice::startNote(int midiNoteNumber, float velocity, SynthesiserSound* s, int /*currentPitchWheelPosition*/)
{
    if (auto* sound = dynamic_cast<const GranularSynthesiserSound*>(s))
    {
        pitchRatio = std::pow(2.0, (midiNoteNumber - sound->midiRootNote) / 12.0) * sound->sampleRate / getSampleRate();

        gain = velocity;
        inRelease = false;

        for (int grainNumber = 0; grainNumber < sound->cloudSize; ++grainNumber)
        {
            auto grainPlaybackSize = grains.size();
            if (grainPlaybackSize == 0 || grainPlaybackSize - 1 < grainNumber)
            {
                addGrain(sound);
            }

            randomizeGrain(grains[grainNumber], *sound);
            grains[grainNumber].envelope.noteOn();
            grains[grainNumber].inRelease = false;
        }
    }
    else
    {
        jassertfalse; // this object can only play GranularSynthesiserVoices!
    }
}

void GranularSynthesiserVoice::stopNote(float /*velocity*/, bool allowTailOff)
{
    for (Grain& grain : grains)
    {
        grain.inRelease = true;
         if (allowTailOff)
         {
             grain.envelope.noteOff();
         }
         else
         {
            grain.envelope.reset();
         }
    }

    inRelease = true;

     if (!allowTailOff)
     {
        clearCurrentNote();
     }
}

void GranularSynthesiserVoice::renderNextBlock(AudioBuffer<float>& outputBuffer, int startSample, int numSamples)
{
    auto* sound = static_cast<GranularSynthesiserSound const*>(getCurrentlyPlayingSound().get());

    if (!sound)
        return;

    int endSample = startSample + numSamples;
    for (int sampleNum = startSample; sampleNum < endSample; ++sampleNum)
    {
        auto monoSample = getNextSample(sound, LEFT_CHANNEL);
        *outputBuffer.getWritePointer(LEFT_CHANNEL, sampleNum) += monoSample;

        if (outputBuffer.getNumChannels() > 1)
        {
            if (sound->sourceAudioBuffer->getNumChannels() > 1) //stereo input and output
            {
                *outputBuffer.getWritePointer(RIGHT_CHANNEL, sampleNum) += getNextSample(sound, RIGHT_CHANNEL);
            }
            else
            { //stereo output, but mono input; just use the same sample for both channels
                *outputBuffer.getWritePointer(RIGHT_CHANNEL, sampleNum) += monoSample;
            }
        }
    }
}

float GranularSynthesiserVoice::getNextSample(GranularSynthesiserSound const* sound, int channel)
{
    float sample = 0;
    int inactiveGrains = 0;

    for (int grainNumber = 0; grainNumber < sound->cloudSize; ++grainNumber)
    {
        if (grains.size() < grainNumber + 1 && !inRelease)
        {
            addGrain(sound);
            randomizeGrain(grains.back(), *sound);
        }
        auto& grain = grains[grainNumber];

        // If the Current Grain Isn't Active
        if (!grain.envelope.isActive())
        {
            inactiveGrains++;
            if (inRelease)
            {
                if (grainNumber = inactiveGrains - 1)
                {
                    //all grains are inactive, clear the note
                    clearCurrentNote();
                }
            }
            else
            {
                randomizeGrain(grain, *sound);
                grain.envelope.noteOn();
                grain.inRelease = false;
            }
        }
        else if (!grain.inRelease)
        {
            // Check if we are finished getting the current grain
            if (sound->sourceAudioBuffer->getNumChannels() >= 2) // Stereo
            {
                if ((grain.currentSample[LEFT_CHANNEL] >= grain.endSample) &&
                    (grain.currentSample[RIGHT_CHANNEL] >= grain.endSample))
                {
                    grain.envelope.noteOff();
                    grain.inRelease = true;
                }
            }
            else if (grain.currentSample[LEFT_CHANNEL] >= grain.endSample) // Mono
            {
                grain.envelope.noteOff();
                grain.inRelease = true;
            }
        }

        // Get the Current Sample From the Audio Buffer
        float currentSample = (sound->sourceAudioBuffer->getSample(channel, static_cast<int>(grain.currentSample[channel])));

        //         currentSample             ADSR Grain                         
        sample += (currentSample * grain.envelope.getNextSample());


        //TODO change pitch without chaning speed?
        grain.currentSample[channel] += this->pitchRatio;
        if (grain.currentSample[channel] >= static_cast<double>(sound->waveSize))
            grain.currentSample[channel] = (static_cast<double>(sound->waveSize) - 1.0f);
    }

    // Scale the Sample by the Gain
    sample *= static_cast<float>(gain);

    //normalize gain for number of grains
    sample /= std::log2(1 + sound->cloudSize);

    // Clip Check
    if (sample > 1.0f)
        sample = 1.0f;
    else if (sample < -1.0f)
        sample = -1.0f;
    return sample;
}

void GranularSynthesiserVoice::randomizeGrain(Grain& grain, const GranularSynthesiserSound& sound)
{
    Random rand = Random();

    // Ensure the Starting Smaple is non-negative
    if (sound.startingOffset == 0 || (sound.centroidSample - sound.startingOffset) <= 0)
    {
        grain.startSample = sound.centroidSample;
    }
    else
    {
        // Randomize the Starting Sample
        grain.startSample = rand.nextInt(Range<int>(sound.centroidSample - sound.startingOffset,
            sound.centroidSample + sound.startingOffset));

        // Clamp the Starting Sample to be Within the WaveTable Range
        if (grain.startSample < 0)
            grain.startSample = 0;
        else if (grain.startSample >= sound.waveSize)
            grain.startSample = (sound.waveSize - 1);
    }


    grain.currentSample[LEFT_CHANNEL] = static_cast<double>(grain.startSample);
    grain.currentSample[RIGHT_CHANNEL] = static_cast<double>(grain.startSample);

    // Clamp the End Sample to be Within the WaveTable Range
    grain.endSample = grain.startSample + sound.sampleDelta;
    if (grain.endSample >= sound.waveSize)
        grain.endSample = (sound.waveSize - 1);
}

void GranularSynthesiserVoice::addGrain(GranularSynthesiserSound const* sound)
{
    grains.push_back(Grain(getSampleRate()));
    auto& newGrain = grains.back();

    //sync grain with first if no offset
    if ((sound->cloudSize >= 1) && (sound->startingOffset == 0))
    {
        auto& masterGrain = grains.front();
        newGrain.currentSample[0] = masterGrain.currentSample[0];
        newGrain.currentSample[1] = masterGrain.currentSample[1];
        newGrain.inRelease = masterGrain.inRelease;
        newGrain.envelope = masterGrain.envelope;
    }
}
