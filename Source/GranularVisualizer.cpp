/*
  ==============================================================================

    GranularVisualizer.cpp
    Created: 14 Jan 2022 10:20:17am
    Author:  Danny

  ==============================================================================
*/

#include "GranularVisualizer.h"
#include "GranularSynthesiserSound.h"
#include "PluginProcessor.h"

GranularVisualizer::GranularVisualizer(SendaiPluginAudioProcessor& processor, AudioThumbnail& thumbnail) : processor(processor), thumbnail(thumbnail)
{
    processor.valueState.addParameterListener(GranularSynthesiserSound::CENTROID_SAMPLE_ID, this);
    processor.valueState.addParameterListener(GranularSynthesiserSound::GRAIN_DURATION_ID, this);
    processor.valueState.addParameterListener(GranularSynthesiserSound::CLOUD_SIZE_ID, this);
    processor.valueState.addParameterListener(GranularSynthesiserSound::RANDOM_OFFSET_ID, this);

    thumbnail.addChangeListener(this);
}

GranularVisualizer::~GranularVisualizer()
{
    processor.valueState.removeParameterListener(GranularSynthesiserSound::CENTROID_SAMPLE_ID, this);
    processor.valueState.removeParameterListener(GranularSynthesiserSound::GRAIN_DURATION_ID, this);
    processor.valueState.removeParameterListener(GranularSynthesiserSound::CLOUD_SIZE_ID, this);
    processor.valueState.removeParameterListener(GranularSynthesiserSound::RANDOM_OFFSET_ID, this);
}


void GranularVisualizer::changeListenerCallback(ChangeBroadcaster* source)
{
    if (source == &thumbnail)
    {
        repaint();
    }
}

void GranularVisualizer::parameterChanged(const String& parameterID, float newValue)
{
    repaint();
}

void GranularVisualizer::paint(juce::Graphics& g)
{
    auto bounds = getLocalBounds();

    // Check if a file has been loaded and draw wavefrom if necessary
    if (thumbnail.getNumChannels() == 0) // if no wavefrom
    {
        g.setColour(Colours::darkgrey);
        g.fillRect(bounds);
        g.setColour(Colours::white);
        g.drawFittedText("No File Loaded", bounds, Justification::centred, 1);
    }
    else // if waveform
    {
        g.setColour(Colours::transparentBlack);
        g.fillRect(bounds);
        g.setColour(Colours::azure);

        // Draw Waveform
        double audioLength = thumbnail.getTotalLength();
        thumbnail.drawChannels(g, bounds, 0.0, audioLength, 1.0f);

        //don't try to draw selection if currently recording
        if (processor.isRecording())
            return;

        int sampleLength = processor.getCurrentSampleLength();
        int sampleRate = processor.getCurrentSampleRate();

        float centroidSample =  (processor.valueState.getParameter(GranularSynthesiserSound::CENTROID_SAMPLE_ID)->getValue() * sampleLength);
        auto randomOffsetParam = processor.valueState.getParameter(GranularSynthesiserSound::RANDOM_OFFSET_ID);
        auto randomOffset = randomOffsetParam->convertFrom0to1(randomOffsetParam->getValue());
        auto grainDurationParam = processor.valueState.getParameter(GranularSynthesiserSound::GRAIN_DURATION_ID);
        double grainDuration = grainDurationParam->convertFrom0to1(grainDurationParam->getValue()) / 1000; // convert to seconds

        // juce::Logger::writeToLog("Centroid sample: " + String(centroidSample));
        // juce::Logger::writeToLog("Random offset: " + String(randomOffset));
        // juce::Logger::writeToLog("Grain duration: " + String(grainDuration));


        // Draw Current Sample Line
        auto audioPosition = (centroidSample / sampleRate);

        // juce::Logger::writeToLog("Audio position: " + String(audioPosition));


        auto drawPosition((audioPosition / audioLength) * bounds.getWidth() + bounds.getX());
        g.setColour(Colours::green);
        g.drawLine(static_cast<float>(drawPosition),
            static_cast<float>(bounds.getY()),
            static_cast<float>(drawPosition),
            static_cast<float>(bounds.getBottom()),
            2.0f);


        // Get the Current Position in the Waveform and Add the Grain Duration
        audioPosition = (centroidSample / sampleRate) + (grainDuration);
        auto endDrawPosition = ((audioPosition / audioLength) * bounds.getWidth() + bounds.getX());

        // Draw End Duration Sample Line
        g.setColour(Colours::red);
        g.drawLine(static_cast<float>(endDrawPosition),
            static_cast<float>(bounds.getY()),
            static_cast<float>(endDrawPosition),
            static_cast<float>(bounds.getBottom()),
            2.0f);

        // Draw a Box Highlighting the Selected Section of the Waveform
        g.setColour(Colour(juce::uint8(0), juce::uint8(0), juce::uint8(255), juce::uint8(127)));
        g.fillRect(Rectangle<float>(juce::Point<float>(static_cast<float>(drawPosition), static_cast<float>(bounds.getY())),
            juce::Point<float>(static_cast<float>(endDrawPosition), static_cast<float>(bounds.getBottom()))));

        // Get Random Starting/Ending Position
        if (randomOffset > 0)
        {
            // Draw Starting Value Range
            auto minRandomPosition = (centroidSample - randomOffset) / sampleRate;
            if (minRandomPosition < 0)
                minRandomPosition = 0;

            auto drawStartingMinPosition = (minRandomPosition / audioLength) * bounds.getWidth() + bounds.getX();

            double maxRandomPosition = (centroidSample + randomOffset);
            if (maxRandomPosition >= sampleLength)
                maxRandomPosition = sampleLength;
            maxRandomPosition /= sampleRate;

            auto drawStartingMaxPosition = (maxRandomPosition / audioLength) * bounds.getWidth() + bounds.getX();

            g.setColour(Colour(juce::uint8(0), juce::uint8(150), juce::uint8(0), juce::uint8(110)));
            g.fillRect(Rectangle<float>(juce::Point<float>(static_cast<float>(drawStartingMinPosition), static_cast<float>(bounds.getY())),
                juce::Point<float>(static_cast<float>(drawStartingMaxPosition), static_cast<float>(bounds.getBottom()))));

            // Draw Ending Range Based on Starting Value
            minRandomPosition = ((centroidSample - randomOffset) / sampleRate) + (grainDuration);
            if (minRandomPosition < 0)
                minRandomPosition = 0;

            drawStartingMinPosition = (minRandomPosition / audioLength) * bounds.getWidth() + bounds.getX();

            maxRandomPosition = (centroidSample + randomOffset);
            if (maxRandomPosition >= sampleLength)
                maxRandomPosition = sampleLength;
            maxRandomPosition /= sampleRate;
            maxRandomPosition += (grainDuration);

            drawStartingMaxPosition = (maxRandomPosition / audioLength) * bounds.getWidth() + bounds.getX();

            g.setColour(Colour(juce::uint8(150), juce::uint8(0), juce::uint8(0), juce::uint8(110)));
            g.fillRect(Rectangle<float>(juce::Point<float>(static_cast<float>(drawStartingMinPosition), static_cast<float>(bounds.getY())),
                juce::Point<float>(static_cast<float>(drawStartingMaxPosition), static_cast<float>(bounds.getBottom()))));
        }
    }
}
